---
layout: markdown_page
title: "GitLab Offboarding"
---

Before starting an offboarding issue, make sure that the team member's resignation or termination has been discussed and cleared with _at least_ the member of the executive team to whom the team member (in)directly reports.
For [involuntary terminations](/handbook/people-operations/#involuntary-terminations), make yourself familiar with the guidelines in the People Operations handbook.

When it is time for offboarding, [create a new **confidential** issue](https://gitlab.com/gitlab-com/peopleops/issues/new) for former team member using the `offboarding` template [from the dropdown](https://docs.gitlab.com/ce/user/project/description_templates.html#using-the-templates) (edit it for applicability to the individual).
Please [update the list](https://gitlab.com/gitlab-com/peopleops/edit/master/.gitlab/issue_templates/offboarding.md) as more steps arise.

Use the [Offboarding Processes](/handbook/offboarding/offboarding-processes) to assist in completing all of the tasks.
