---
layout: markdown_page
title: "Incentives at GitLab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The following incentives are available for GitLabbers. Also see our separate page on [benefits](/handbook/benefits/) available to GitLabbers.

### Sales Target Dinner Evangelism Reward

Since reaching sales targets is a team effort that integrates everything from making a great product
to providing top notch customer support and everything in between, we reward **all**
GitLabbers (not just the Sales team) for every month that we reach our Sales Targets. The incentive is [100 USD](https://www.google.com/search?q=100+usd+in+eur)
to each team member for the purpose of evangelizing the GitLab story.  You may use the incentive at a restaurant of your choice. Enjoy!

- The CEO, or CRO will announce on the team call if the target was met.
- To claim the incentive, please submit your receipt through Expensify or include on your contractor invoice as a reimbursable expense.
- Indicate on your receipt and in the comment section in expensify "GitLab evangelism" and the names of the other participants.
- You should spend the incentive on eating out, and can be reimbursed _up to_ the maximum of 100 USD.
- Use the incentive in the month following the announcement. So for example, if we reach our target in March, use your incentive in April.
- If you cannot, or decide not to, use the incentive in the expected month, you can carry it over to the next month by notifying [Accounts Payable](mailto:ap@gitlab.com) before the 22nd of the month (release day!). You can only carry over one month in this way.


### Discretionary Bonuses

1. Every now and then, individual GitLabbers really shine as they go above and beyond their regular responsibilities and tasks.
   * We recognize this through the #thanks channel, and sometimes also through a discretionary bonus.
   * Managers can recommend their team members to the CEO for a $1,000 bonus through BambooHR.
   * Once approved, on a team call, the manager announces the “who” and “why” of the bonus; and the "why"
   should be tied to our [values](#values).
1. If you think you are meeting the requirements for another title, want to change
jobs within the company, or think your growth should be reflected in your compensation please feel free to discuss with your manager.

#### Process for Recommending a Team Member for a Bonus in BambooHR

Manager (Reports To):

1. Login to BambooHR.
1. Select the team member you would like to adjust.
1. In the top right hand corner, click Request a Change.
1. Select Bonus.
1. Enter in all applicable fields in the form, and then submit.

People Operations will also process in the applicable payroll.

#### Enter a Bonus into TriNet

1. Go to HR Passport homepage
1. Under "My Company" select "Payroll Entry & Admin"
1. Select the proper payroll
1. Under Other Earnings, select the BNO code and enter the amount of the bonus.
1. **Never** hit submit. (It will cause the entire payroll to be paid the next day)
1. Click Save when done.
Note: Make sure to file all appropriate documentation in BambooHR. Also, if the employee has been employed for less than six months, check the notes section in BambooHR to see if they were referred by anyone at GitLab. If so, process a discretionary bonus for that team member as well.

### Referral Bonuses

Chances are that if you work at GitLab, you have great friends and peers who would
also be fantastic additions to our [Team](https://about.gitlab.com/team/) and who
may be interested in one of the current [Job Openings](https://about.gitlab.com/jobs/).
To help us grow the team with exceptional people, we have referral bonuses that work as follows:

1. We want to encourage and support [diversity](https://about.gitlab.com/handbook/values) on our team and in our hiring practices, so we will offer a $2000 referral bonus for hires from [underrepresented groups in the tech industry](http://seldo.com/weblog/2014/06/25/a_comparison_of_diversity_at_three_major_tech_companies) for engineering roles at GitLab. This underrepresented group is defined as: women, African-Americans, Hispanic-Americans/Latinos, and veterans.
1. Any great candidate that is referred and hired will earn a GitLab employee a $1,000 bonus
once the new team member has been with the company for 3 months.
1. Exceptions: no bonuses for hiring people who report to you, and no bonus for the executive team.
1. When your referral applies for an opening, make sure that they enter your name on the application form.
1. You can also submit passive referrals for our [Global Recruiters](https://about.gitlab.com/jobs/recruiter) to actively connect with via the passive [referral form](https://goo.gl/forms/1rNIYpdgDB3qXBAi2)

People Ops will process the bonus.

#### Document a future bonus in BambooHR

1. Go to the employee who referred the new team member in BambooHR
1. Under the Jobs Tab click Update Bonus
1. Add the date the bonus will be paid if all conditions are met (90 days from hire of new employee)
1. Enter the bonus amount of $1,000
1. Enter the bonus type to be a Referral Bonus
1. Enter a note stating that this is a future bonus (this will be changed once the bonus has been paid)

#### Notification to Process a Bonus

BambooHR will send an email to PeopleOps on the date that the referral bonus should be paid for processing through the applicable payroll system. If the team member is a contractor, send them an email (and cc Finance) to invoice the bonus.
Once the bonus has been processed, change the note in BambooHR to denote the referral bonus has been paid.

### Work Remotely Travel Grant

GitLab is a [remote-first company](http://zachholman.com/posts/remote-first/) with GitLabbers all over the world (see the map on our [Team page](https://about.gitlab.com/team/) ). If you want to visit a colleague in another part of the world, or promote GitLab at events in another country, then present your travel plan to your manager or the CEO, and you can receive *up to* $2,000 in support for your plan!

As an example, the first grant was handed to a team member who will be traveling to 6 GitLabbers in different countries during 6 months, and this team member will receive the maximum grant of $2,000.

To claim the approved award, include a line item on your expense report or invoice with the approval email as the receipt. The entire award can be claimed from the first month of travel to up to 3 months after the travel is completed.
